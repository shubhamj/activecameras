% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 1425.593925591334482 ; 1463.311612981741291 ];

%-- Principal point:
cc = [ 479.607863439129460 ; 623.668060821558470 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.040415807305361 ; -0.064321392897523 ; 0.057514607411832 ; -0.012860150845197 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 186.494723581658178 ; 230.013221749295894 ];

%-- Principal point uncertainty:
cc_error = [ 103.479785322244965 ; 173.667967903412233 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.252624999256590 ; 0.398782750318482 ; 0.058703820939558 ; 0.014426010824817 ; 0.000000000000000 ];

%-- Image size:
nx = 1280;
ny = 720;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 41;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -1.974843e+00 ; -1.741306e+00 ; -3.179041e-01 ];
Tc_1  = [ 3.267614e+03 ; -4.190816e+03 ; 1.350270e+04 ];
omc_error_1 = [ 7.796789e-02 ; 8.663344e-02 ; 1.554410e-01 ];
Tc_error_1  = [ 1.018807e+03 ; 1.665057e+03 ; 1.854594e+03 ];

%-- Image #2:
omc_2 = [ -2.420298e+00 ; -9.992434e-01 ; 7.323180e-01 ];
Tc_2  = [ 3.657188e+03 ; -3.917422e+03 ; 1.310482e+04 ];
omc_error_2 = [ 1.066537e-01 ; 2.927397e-02 ; 1.548944e-01 ];
Tc_error_2  = [ 1.005456e+03 ; 1.606490e+03 ; 1.764095e+03 ];

%-- Image #3:
omc_3 = [ -1.813770e+00 ; -2.314055e+00 ; -5.985495e-01 ];
Tc_3  = [ 5.528061e+02 ; -3.307258e+03 ; 1.182717e+04 ];
omc_error_3 = [ 5.151432e-02 ; 8.579739e-02 ; 1.685686e-01 ];
Tc_error_3  = [ 8.894281e+02 ; 1.407196e+03 ; 1.454541e+03 ];

%-- Image #4:
omc_4 = [ 1.893583e+00 ; 1.795961e+00 ; 8.139409e-01 ];
Tc_4  = [ 3.884457e+02 ; -3.327355e+03 ; 1.181112e+04 ];
omc_error_4 = [ 6.990905e-02 ; 6.001225e-02 ; 1.416801e-01 ];
Tc_error_4  = [ 8.899803e+02 ; 1.410260e+03 ; 1.495207e+03 ];

%-- Image #5:
omc_5 = [ -1.802196e+00 ; -1.733294e+00 ; 5.614414e-01 ];
Tc_5  = [ 1.168855e+03 ; -3.319130e+03 ; 1.261781e+04 ];
omc_error_5 = [ 7.176453e-02 ; 3.992090e-02 ; 1.489810e-01 ];
Tc_error_5  = [ 9.479021e+02 ; 1.502549e+03 ; 1.541180e+03 ];

%-- Image #6:
omc_6 = [ 1.773097e+00 ; 1.762158e+00 ; -8.389266e-01 ];
Tc_6  = [ 1.084785e+03 ; -3.029229e+03 ; 1.372660e+04 ];
omc_error_6 = [ 8.296293e-02 ; 7.523644e-02 ; 1.266776e-01 ];
Tc_error_6  = [ 1.028181e+03 ; 1.627808e+03 ; 1.660209e+03 ];

%-- Image #7:
omc_7 = [ 2.102812e+00 ; 2.023963e+00 ; 2.094234e-01 ];
Tc_7  = [ 4.300782e+02 ; -3.658315e+03 ; 1.356698e+04 ];
omc_error_7 = [ 7.059950e-02 ; 6.203176e-02 ; 1.574042e-01 ];
Tc_error_7  = [ 1.021740e+03 ; 1.618728e+03 ; 1.705512e+03 ];

%-- Image #8:
omc_8 = [ 2.086404e+00 ; 2.038699e+00 ; -5.022204e-02 ];
Tc_8  = [ 3.994765e+02 ; -3.780692e+03 ; 1.409376e+04 ];
omc_error_8 = [ 6.865136e-02 ; 6.706258e-02 ; 1.575162e-01 ];
Tc_error_8  = [ 1.061103e+03 ; 1.679132e+03 ; 1.752159e+03 ];

%-- Image #9:
omc_9 = [ 2.114059e+00 ; 1.943720e+00 ; -3.771379e-01 ];
Tc_9  = [ 4.630053e+02 ; -2.593440e+03 ; 1.457424e+04 ];
omc_error_9 = [ 6.932978e-02 ; 7.032979e-02 ; 1.512442e-01 ];
Tc_error_9  = [ 1.082398e+03 ; 1.722304e+03 ; 1.772468e+03 ];

%-- Image #10:
omc_10 = [ 2.384575e+00 ; 2.070576e+00 ; -3.743914e-01 ];
Tc_10  = [ 1.158441e+03 ; -3.274574e+03 ; 1.469794e+04 ];
omc_error_10 = [ 6.846988e-02 ; 7.565119e-02 ; 1.801889e-01 ];
Tc_error_10  = [ 1.099346e+03 ; 1.743058e+03 ; 1.781669e+03 ];

%-- Image #11:
omc_11 = [ 2.377481e+00 ; 2.083302e+00 ; -3.758077e-01 ];
Tc_11  = [ 1.212248e+03 ; -3.265197e+03 ; 1.475996e+04 ];
omc_error_11 = [ 6.822344e-02 ; 7.594473e-02 ; 1.808489e-01 ];
Tc_error_11  = [ 1.103826e+03 ; 1.750540e+03 ; 1.789954e+03 ];

%-- Image #12:
omc_12 = [ -2.748848e+00 ; -1.129064e+00 ; 7.991081e-01 ];
Tc_12  = [ 1.646058e+03 ; -3.500275e+03 ; 1.436838e+04 ];
omc_error_12 = [ 9.041002e-02 ; 1.241661e-02 ; 1.625379e-01 ];
Tc_error_12  = [ 1.074698e+03 ; 1.710529e+03 ; 1.749200e+03 ];

%-- Image #13:
omc_13 = [ -1.662770e+00 ; -2.341557e+00 ; -2.734277e-02 ];
Tc_13  = [ 1.109699e+03 ; -4.179728e+03 ; 1.423043e+04 ];
omc_error_13 = [ 8.717990e-02 ; 9.096782e-02 ; 2.202263e-01 ];
Tc_error_13  = [ 1.073968e+03 ; 1.696332e+03 ; 1.759102e+03 ];

%-- Image #14:
omc_14 = [ -1.814410e+00 ; -2.174740e+00 ; -8.591700e-02 ];
Tc_14  = [ 1.051172e+03 ; -4.599595e+03 ; 1.409951e+04 ];
omc_error_14 = [ 1.073162e-01 ; 9.772066e-02 ; 2.378821e-01 ];
Tc_error_14  = [ 1.072032e+03 ; 1.683887e+03 ; 1.772676e+03 ];

%-- Image #15:
omc_15 = [ -1.688690e+00 ; -2.056704e+00 ; -3.856367e-01 ];
Tc_15  = [ 8.040928e+02 ; -4.746874e+03 ; 1.372848e+04 ];
omc_error_15 = [ 6.438588e-02 ; 6.090499e-02 ; 1.544023e-01 ];
Tc_error_15  = [ 1.049574e+03 ; 1.656761e+03 ; 1.751570e+03 ];

%-- Image #16:
omc_16 = [ -2.084475e+00 ; -1.764809e+00 ; -5.880706e-01 ];
Tc_16  = [ 1.895952e+02 ; -4.772840e+03 ; 1.351455e+04 ];
omc_error_16 = [ 6.773914e-02 ; 6.866690e-02 ; 1.483485e-01 ];
Tc_error_16  = [ 1.030531e+03 ; 1.623284e+03 ; 1.693737e+03 ];

%-- Image #17:
omc_17 = [ -2.218673e+00 ; -1.907092e+00 ; -3.560597e-01 ];
Tc_17  = [ 2.811691e+02 ; -4.056736e+03 ; 1.349407e+04 ];
omc_error_17 = [ 1.196224e-01 ; 1.040588e-01 ; 2.275089e-01 ];
Tc_error_17  = [ 1.019072e+03 ; 1.605296e+03 ; 1.675445e+03 ];

%-- Image #18:
omc_18 = [ -1.714399e+00 ; -1.461928e+00 ; -8.989298e-01 ];
Tc_18  = [ 2.893738e+02 ; -4.531919e+03 ; 1.323746e+04 ];
omc_error_18 = [ 7.557660e-02 ; 6.949655e-02 ; 1.139295e-01 ];
Tc_error_18  = [ 1.007948e+03 ; 1.595267e+03 ; 1.670256e+03 ];

%-- Image #19:
omc_19 = [ -2.069993e+00 ; -1.889006e+00 ; -1.342393e-01 ];
Tc_19  = [ 4.304186e+02 ; -5.583801e+03 ; 1.346618e+04 ];
omc_error_19 = [ 1.650848e-01 ; 1.132417e-01 ; 2.285510e-01 ];
Tc_error_19  = [ 1.041125e+03 ; 1.623005e+03 ; 1.769524e+03 ];

%-- Image #20:
omc_20 = [ -2.295330e+00 ; -2.052743e+00 ; 1.924134e-01 ];
Tc_20  = [ 6.518464e+02 ; -1.833277e+03 ; 1.518207e+04 ];
omc_error_20 = [ 1.140447e-01 ; 9.085049e-02 ; 2.220440e-01 ];
Tc_error_20  = [ 1.117273e+03 ; 1.794196e+03 ; 1.849056e+03 ];

%-- Image #21:
omc_21 = [ 2.019768e+00 ; 1.783271e+00 ; 5.342176e-01 ];
Tc_21  = [ 4.710457e+02 ; -1.821108e+03 ; 1.491346e+04 ];
omc_error_21 = [ 8.073405e-02 ; 4.893361e-02 ; 1.367371e-01 ];
Tc_error_21  = [ 1.095737e+03 ; 1.773466e+03 ; 1.921715e+03 ];

%-- Image #22:
omc_22 = [ 2.149882e+00 ; 1.903279e+00 ; -3.935377e-01 ];
Tc_22  = [ 5.148146e+02 ; -1.847954e+03 ; 1.487996e+04 ];
omc_error_22 = [ 7.168556e-02 ; 7.119833e-02 ; 1.520044e-01 ];
Tc_error_22  = [ 1.097335e+03 ; 1.756771e+03 ; 1.814675e+03 ];

%-- Image #23:
omc_23 = [ -2.253228e+00 ; -1.915103e+00 ; -2.693791e-01 ];
Tc_23  = [ 6.610867e+02 ; -1.925686e+03 ; 1.461993e+04 ];
omc_error_23 = [ 1.159197e-01 ; 1.060901e-01 ; 1.839630e-01 ];
Tc_error_23  = [ 1.074451e+03 ; 1.729179e+03 ; 1.776295e+03 ];

%-- Image #24:
omc_24 = [ -2.108697e+00 ; -1.960703e+00 ; -4.335371e-01 ];
Tc_24  = [ 6.318685e+02 ; -3.263577e+03 ; 1.452209e+04 ];
omc_error_24 = [ 8.044403e-02 ; 8.477470e-02 ; 1.665943e-01 ];
Tc_error_24  = [ 1.082808e+03 ; 1.721704e+03 ; 1.761514e+03 ];

%-- Image #25:
omc_25 = [ -2.085689e+00 ; -1.946277e+00 ; -4.489138e-01 ];
Tc_25  = [ 6.283223e+02 ; -3.228517e+03 ; 1.451553e+04 ];
omc_error_25 = [ 7.751665e-02 ; 8.166942e-02 ; 1.641026e-01 ];
Tc_error_25  = [ 1.082449e+03 ; 1.720256e+03 ; 1.761748e+03 ];

%-- Image #26:
omc_26 = [ -2.181530e+00 ; -2.043005e+00 ; -3.181003e-01 ];
Tc_26  = [ 5.938981e+02 ; -2.142100e+03 ; 1.467947e+04 ];
omc_error_26 = [ 1.055328e-01 ; 1.074837e-01 ; 1.727950e-01 ];
Tc_error_26  = [ 1.080118e+03 ; 1.738592e+03 ; 1.790918e+03 ];

%-- Image #27:
omc_27 = [ -2.170295e+00 ; -2.064099e+00 ; -3.541501e-01 ];
Tc_27  = [ 6.277375e+02 ; -2.016882e+03 ; 1.469055e+04 ];
omc_error_27 = [ 9.650242e-02 ; 1.034723e-01 ; 1.650289e-01 ];
Tc_error_27  = [ 1.079401e+03 ; 1.740132e+03 ; 1.798752e+03 ];

%-- Image #28:
omc_28 = [ 2.575433e+00 ; 1.744297e+00 ; -3.779882e-01 ];
Tc_28  = [ 1.328992e+03 ; -2.086899e+03 ; 1.484565e+04 ];
omc_error_28 = [ 8.467110e-02 ; 6.706103e-02 ; 1.827541e-01 ];
Tc_error_28  = [ 1.098161e+03 ; 1.756736e+03 ; 1.804521e+03 ];

%-- Image #29:
omc_29 = [ 2.873300e+00 ; 9.093709e-01 ; -3.891467e-01 ];
Tc_29  = [ 1.843757e+03 ; -2.925016e+03 ; 1.505852e+04 ];
omc_error_29 = [ 1.053004e-01 ; 3.992465e-02 ; 1.546102e-01 ];
Tc_error_29  = [ 1.124177e+03 ; 1.791552e+03 ; 1.857287e+03 ];

%-- Image #30:
omc_30 = [ -1.611413e+00 ; -2.532421e+00 ; -6.048981e-01 ];
Tc_30  = [ -1.122791e+02 ; -3.546973e+03 ; 1.421139e+04 ];
omc_error_30 = [ 5.354963e-02 ; 9.319666e-02 ; 1.756120e-01 ];
Tc_error_30  = [ 1.065029e+03 ; 1.688561e+03 ; 1.736228e+03 ];

%-- Image #31:
omc_31 = [ 2.138878e+00 ; 2.444009e+00 ; 6.356644e-01 ];
Tc_31  = [ 8.854325e+02 ; -4.871263e+03 ; 1.377497e+04 ];
omc_error_31 = [ 5.800314e-02 ; 6.956734e-02 ; 2.094650e-01 ];
Tc_error_31  = [ 1.051149e+03 ; 1.646140e+03 ; 1.756469e+03 ];

%-- Image #32:
omc_32 = [ -1.505139e+00 ; -1.671508e+00 ; -7.564924e-01 ];
Tc_32  = [ 8.087187e+02 ; -4.540938e+03 ; 1.339289e+04 ];
omc_error_32 = [ 6.897032e-02 ; 6.434712e-02 ; 1.212885e-01 ];
Tc_error_32  = [ 1.021858e+03 ; 1.616803e+03 ; 1.706861e+03 ];

%-- Image #33:
omc_33 = [ -1.543304e+00 ; -1.751193e+00 ; -6.680070e-01 ];
Tc_33  = [ 8.364866e+02 ; -4.688083e+03 ; 1.353270e+04 ];
omc_error_33 = [ 6.536854e-02 ; 6.142477e-02 ; 1.254662e-01 ];
Tc_error_33  = [ 1.034190e+03 ; 1.635550e+03 ; 1.727585e+03 ];

%-- Image #34:
omc_34 = [ -1.473556e+00 ; -2.568519e+00 ; 4.550086e-01 ];
Tc_34  = [ 1.538838e+03 ; -4.722006e+03 ; 1.459173e+04 ];
omc_error_34 = [ 6.483283e-02 ; 5.745770e-02 ; 1.770124e-01 ];
Tc_error_34  = [ 1.109176e+03 ; 1.749496e+03 ; 1.808938e+03 ];

%-- Image #35:
omc_35 = [ -2.484896e+00 ; -1.648075e+00 ; 4.685543e-01 ];
Tc_35  = [ 1.195578e+03 ; -3.927566e+03 ; 1.460605e+04 ];
omc_error_35 = [ 8.755366e-02 ; 3.292425e-02 ; 1.722640e-01 ];
Tc_error_35  = [ 1.098737e+03 ; 1.737897e+03 ; 1.776814e+03 ];

%-- Image #36:
omc_36 = [ -1.740518e+00 ; -2.464302e+00 ; -5.640493e-01 ];
Tc_36  = [ -4.279175e+02 ; -3.837688e+03 ; 1.414713e+04 ];
omc_error_36 = [ 6.866819e-02 ; 9.861136e-02 ; 1.822259e-01 ];
Tc_error_36  = [ 1.064894e+03 ; 1.684622e+03 ; 1.734896e+03 ];

%-- Image #37:
omc_37 = [ -1.695788e+00 ; -2.484036e+00 ; -5.653940e-01 ];
Tc_37  = [ -3.844096e+02 ; -3.843707e+03 ; 1.414530e+04 ];
omc_error_37 = [ 6.702845e-02 ; 9.839188e-02 ; 1.833992e-01 ];
Tc_error_37  = [ 1.064687e+03 ; 1.684102e+03 ; 1.732958e+03 ];

%-- Image #38:
omc_38 = [ -2.528783e+00 ; -1.546316e+00 ; -3.810258e-01 ];
Tc_38  = [ 3.167375e+01 ; -2.330604e+03 ; 1.457930e+04 ];
omc_error_38 = [ 1.159454e-01 ; 8.196901e-02 ; 1.285394e-01 ];
Tc_error_38  = [ 1.078032e+03 ; 1.729735e+03 ; 1.789321e+03 ];

%-- Image #39:
omc_39 = [ -1.620945e+00 ; -1.951478e+00 ; -7.169315e-01 ];
Tc_39  = [ 1.060703e+03 ; -4.956165e+03 ; 1.309701e+04 ];
omc_error_39 = [ 5.246180e-02 ; 6.595210e-02 ; 1.342782e-01 ];
Tc_error_39  = [ 1.006514e+03 ; 1.594007e+03 ; 1.684636e+03 ];

%-- Image #40:
omc_40 = [ -1.634414e+00 ; -1.882274e+00 ; -5.630223e-01 ];
Tc_40  = [ 1.139801e+03 ; -4.992506e+03 ; 1.298263e+04 ];
omc_error_40 = [ 5.760968e-02 ; 5.893926e-02 ; 1.305929e-01 ];
Tc_error_40  = [ 1.000903e+03 ; 1.584075e+03 ; 1.677753e+03 ];

%-- Image #41:
omc_41 = [ 1.368959e+00 ; 2.710214e+00 ; 5.552879e-01 ];
Tc_41  = [ -3.547554e+03 ; -6.697668e+02 ; 1.260009e+04 ];
omc_error_41 = [ 6.632688e-02 ; 1.403090e-01 ; 2.578870e-01 ];
Tc_error_41  = [ 9.886228e+02 ; 1.527070e+03 ; 1.847193e+03 ];

