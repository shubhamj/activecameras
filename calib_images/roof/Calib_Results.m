% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 574.298752408322230 ; 574.298752408322230 ];

%-- Principal point:
cc = [ 639.500000000000000 ; 359.500000000000000 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.000000000000000 ; 0.000000000000000 ; 0.000000000000000 ; 0.000000000000000 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 186.494723581658178 ; 230.013221749295894 ];

%-- Principal point uncertainty:
cc_error = [ 103.479785322244965 ; 173.667967903412233 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.252624999256590 ; 0.398782750318482 ; 0.058703820939558 ; 0.014426010824817 ; 0.000000000000000 ];

%-- Image size:
nx = 1280;
ny = 720;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 41;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.191682e+00 ; 1.650899e+00 ; -1.597405e+00 ];
Tc_1  = [ 1.731015e+03 ; -1.280730e+03 ; 5.527997e+03 ];
omc_error_1 = [ 7.796789e-02 ; 8.663344e-02 ; 1.554410e-01 ];
Tc_error_1  = [ 1.018807e+03 ; 1.665057e+03 ; 1.854594e+03 ];

%-- Image #2:
omc_2 = [ NaN ; NaN ; NaN ];
Tc_2  = [ NaN ; NaN ; NaN ];
omc_error_2 = [ 1.066537e-01 ; 2.927397e-02 ; 1.548944e-01 ];
Tc_error_2  = [ 1.005456e+03 ; 1.606490e+03 ; 1.764095e+03 ];

%-- Image #3:
omc_3 = [ NaN ; NaN ; NaN ];
Tc_3  = [ NaN ; NaN ; NaN ];
omc_error_3 = [ 5.151432e-02 ; 8.579739e-02 ; 1.685686e-01 ];
Tc_error_3  = [ 8.894281e+02 ; 1.407196e+03 ; 1.454541e+03 ];

%-- Image #4:
omc_4 = [ NaN ; NaN ; NaN ];
Tc_4  = [ NaN ; NaN ; NaN ];
omc_error_4 = [ 6.990905e-02 ; 6.001225e-02 ; 1.416801e-01 ];
Tc_error_4  = [ 8.899803e+02 ; 1.410260e+03 ; 1.495207e+03 ];

%-- Image #5:
omc_5 = [ 2.397675e+00 ; -2.958878e+00 ; -2.434036e+00 ];
Tc_5  = [ 2.280237e+02 ; 6.221694e+02 ; -4.947400e+03 ];
omc_error_5 = [ 7.176453e-02 ; 3.992090e-02 ; 1.489810e-01 ];
Tc_error_5  = [ 9.479021e+02 ; 1.502549e+03 ; 1.541180e+03 ];

%-- Image #6:
omc_6 = [ NaN ; NaN ; NaN ];
Tc_6  = [ NaN ; NaN ; NaN ];
omc_error_6 = [ 8.296293e-02 ; 7.523644e-02 ; 1.266776e-01 ];
Tc_error_6  = [ 1.028181e+03 ; 1.627808e+03 ; 1.660209e+03 ];

%-- Image #7:
omc_7 = [ NaN ; NaN ; NaN ];
Tc_7  = [ NaN ; NaN ; NaN ];
omc_error_7 = [ 7.059950e-02 ; 6.203176e-02 ; 1.574042e-01 ];
Tc_error_7  = [ 1.021740e+03 ; 1.618728e+03 ; 1.705512e+03 ];

%-- Image #8:
omc_8 = [ NaN ; NaN ; NaN ];
Tc_8  = [ NaN ; NaN ; NaN ];
omc_error_8 = [ 6.865136e-02 ; 6.706258e-02 ; 1.575162e-01 ];
Tc_error_8  = [ 1.061103e+03 ; 1.679132e+03 ; 1.752159e+03 ];

%-- Image #9:
omc_9 = [ NaN ; NaN ; NaN ];
Tc_9  = [ NaN ; NaN ; NaN ];
omc_error_9 = [ 6.932978e-02 ; 7.032979e-02 ; 1.512442e-01 ];
Tc_error_9  = [ 1.082398e+03 ; 1.722304e+03 ; 1.772468e+03 ];

%-- Image #10:
omc_10 = [ 1.188828e+01 ; -1.401649e+01 ; 9.647572e+00 ];
Tc_10  = [ 3.869757e+02 ; 2.412855e+02 ; -5.552455e+03 ];
omc_error_10 = [ 6.846988e-02 ; 7.565119e-02 ; 1.801889e-01 ];
Tc_error_10  = [ 1.099346e+03 ; 1.743058e+03 ; 1.781669e+03 ];

%-- Image #11:
omc_11 = [ NaN ; NaN ; NaN ];
Tc_11  = [ NaN ; NaN ; NaN ];
omc_error_11 = [ 6.822344e-02 ; 7.594473e-02 ; 1.808489e-01 ];
Tc_error_11  = [ 1.103826e+03 ; 1.750540e+03 ; 1.789954e+03 ];

%-- Image #12:
omc_12 = [ NaN ; NaN ; NaN ];
Tc_12  = [ NaN ; NaN ; NaN ];
omc_error_12 = [ 9.041002e-02 ; 1.241661e-02 ; 1.625379e-01 ];
Tc_error_12  = [ 1.074698e+03 ; 1.710529e+03 ; 1.749200e+03 ];

%-- Image #13:
omc_13 = [ 1.445251e+00 ; -1.187498e+00 ; 1.169276e+00 ];
Tc_13  = [ 7.611059e+02 ; 1.395875e+03 ; -7.245068e+03 ];
omc_error_13 = [ 8.717990e-02 ; 9.096782e-02 ; 2.202263e-01 ];
Tc_error_13  = [ 1.073968e+03 ; 1.696332e+03 ; 1.759102e+03 ];

%-- Image #14:
omc_14 = [ NaN ; NaN ; NaN ];
Tc_14  = [ NaN ; NaN ; NaN ];
omc_error_14 = [ 1.073162e-01 ; 9.772066e-02 ; 2.378821e-01 ];
Tc_error_14  = [ 1.072032e+03 ; 1.683887e+03 ; 1.772676e+03 ];

%-- Image #15:
omc_15 = [ 1.675757e+00 ; 2.240749e+00 ; 3.107412e+00 ];
Tc_15  = [ -9.615105e+02 ; -2.020226e+03 ; 6.505014e+03 ];
omc_error_15 = [ 6.438588e-02 ; 6.090499e-02 ; 1.544023e-01 ];
Tc_error_15  = [ 1.049574e+03 ; 1.656761e+03 ; 1.751570e+03 ];

%-- Image #16:
omc_16 = [ -1.928784e+01 ; 2.300525e+01 ; -7.323238e+00 ];
Tc_16  = [ 1.505725e+03 ; 2.249117e+03 ; -6.598492e+03 ];
omc_error_16 = [ 6.773914e-02 ; 6.866690e-02 ; 1.483485e-01 ];
Tc_error_16  = [ 1.030531e+03 ; 1.623284e+03 ; 1.693737e+03 ];

%-- Image #17:
omc_17 = [ NaN ; NaN ; NaN ];
Tc_17  = [ NaN ; NaN ; NaN ];
omc_error_17 = [ 1.196224e-01 ; 1.040588e-01 ; 2.275089e-01 ];
Tc_error_17  = [ 1.019072e+03 ; 1.605296e+03 ; 1.675445e+03 ];

%-- Image #18:
omc_18 = [ NaN ; NaN ; NaN ];
Tc_18  = [ NaN ; NaN ; NaN ];
omc_error_18 = [ 7.557660e-02 ; 6.949655e-02 ; 1.139295e-01 ];
Tc_error_18  = [ 1.007948e+03 ; 1.595267e+03 ; 1.670256e+03 ];

%-- Image #19:
omc_19 = [ NaN ; NaN ; NaN ];
Tc_19  = [ NaN ; NaN ; NaN ];
omc_error_19 = [ 1.650848e-01 ; 1.132417e-01 ; 2.285510e-01 ];
Tc_error_19  = [ 1.041125e+03 ; 1.623005e+03 ; 1.769524e+03 ];

%-- Image #20:
omc_20 = [ 1.244728e+00 ; 9.545101e-01 ; -1.463872e+00 ];
Tc_20  = [ -8.910853e+02 ; 1.159580e+03 ; 5.711403e+03 ];
omc_error_20 = [ 1.140447e-01 ; 9.085049e-02 ; 2.220440e-01 ];
Tc_error_20  = [ 1.117273e+03 ; 1.794196e+03 ; 1.849056e+03 ];

%-- Image #21:
omc_21 = [ NaN ; NaN ; NaN ];
Tc_21  = [ NaN ; NaN ; NaN ];
omc_error_21 = [ 8.073405e-02 ; 4.893361e-02 ; 1.367371e-01 ];
Tc_error_21  = [ 1.095737e+03 ; 1.773466e+03 ; 1.921715e+03 ];

%-- Image #22:
omc_22 = [ NaN ; NaN ; NaN ];
Tc_22  = [ NaN ; NaN ; NaN ];
omc_error_22 = [ 7.168556e-02 ; 7.119833e-02 ; 1.520044e-01 ];
Tc_error_22  = [ 1.097335e+03 ; 1.756771e+03 ; 1.814675e+03 ];

%-- Image #23:
omc_23 = [ 1.289232e+00 ; 9.655552e-01 ; -1.482628e+00 ];
Tc_23  = [ -8.245794e+02 ; 9.845592e+02 ; 5.561108e+03 ];
omc_error_23 = [ 1.159197e-01 ; 1.060901e-01 ; 1.839630e-01 ];
Tc_error_23  = [ 1.074451e+03 ; 1.729179e+03 ; 1.776295e+03 ];

%-- Image #24:
omc_24 = [ NaN ; NaN ; NaN ];
Tc_24  = [ NaN ; NaN ; NaN ];
omc_error_24 = [ 8.044403e-02 ; 8.477470e-02 ; 1.665943e-01 ];
Tc_error_24  = [ 1.082808e+03 ; 1.721704e+03 ; 1.761514e+03 ];

%-- Image #25:
omc_25 = [ NaN ; NaN ; NaN ];
Tc_25  = [ NaN ; NaN ; NaN ];
omc_error_25 = [ 7.751665e-02 ; 8.166942e-02 ; 1.641026e-01 ];
Tc_error_25  = [ 1.082449e+03 ; 1.720256e+03 ; 1.761748e+03 ];

%-- Image #26:
omc_26 = [ NaN ; NaN ; NaN ];
Tc_26  = [ NaN ; NaN ; NaN ];
omc_error_26 = [ 1.055328e-01 ; 1.074837e-01 ; 1.727950e-01 ];
Tc_error_26  = [ 1.080118e+03 ; 1.738592e+03 ; 1.790918e+03 ];

%-- Image #27:
omc_27 = [ 9.806831e-01 ; -1.163823e+00 ; 1.148908e+00 ];
Tc_27  = [ 8.996243e+02 ; -9.286876e+02 ; -5.620381e+03 ];
omc_error_27 = [ 9.650242e-02 ; 1.034723e-01 ; 1.650289e-01 ];
Tc_error_27  = [ 1.079401e+03 ; 1.740132e+03 ; 1.798752e+03 ];

%-- Image #28:
omc_28 = [ NaN ; NaN ; NaN ];
Tc_28  = [ NaN ; NaN ; NaN ];
omc_error_28 = [ 8.467110e-02 ; 6.706103e-02 ; 1.827541e-01 ];
Tc_error_28  = [ 1.098161e+03 ; 1.756736e+03 ; 1.804521e+03 ];

%-- Image #29:
omc_29 = [ 2.715758e+00 ; 8.314356e-01 ; -2.846671e-01 ];
Tc_29  = [ 1.032122e+02 ; -9.800505e+01 ; 6.212102e+03 ];
omc_error_29 = [ 1.053004e-01 ; 3.992465e-02 ; 1.546102e-01 ];
Tc_error_29  = [ 1.124177e+03 ; 1.791552e+03 ; 1.857287e+03 ];

%-- Image #30:
omc_30 = [ NaN ; NaN ; NaN ];
Tc_30  = [ NaN ; NaN ; NaN ];
omc_error_30 = [ 5.354963e-02 ; 9.319666e-02 ; 1.756120e-01 ];
Tc_error_30  = [ 1.065029e+03 ; 1.688561e+03 ; 1.736228e+03 ];

%-- Image #31:
omc_31 = [ 1.399326e+00 ; 1.471259e+00 ; -1.127534e+00 ];
Tc_31  = [ -9.458378e+02 ; -2.354079e+03 ; 7.191583e+03 ];
omc_error_31 = [ 5.800314e-02 ; 6.956734e-02 ; 2.094650e-01 ];
Tc_error_31  = [ 1.051149e+03 ; 1.646140e+03 ; 1.756469e+03 ];

%-- Image #32:
omc_32 = [ -2.379549e+00 ; -2.516290e+00 ; 1.944326e+00 ];
Tc_32  = [ -9.365571e+02 ; -2.137298e+03 ; 6.981171e+03 ];
omc_error_32 = [ 6.897032e-02 ; 6.434712e-02 ; 1.212885e-01 ];
Tc_error_32  = [ 1.021858e+03 ; 1.616803e+03 ; 1.706861e+03 ];

%-- Image #33:
omc_33 = [ NaN ; NaN ; NaN ];
Tc_33  = [ NaN ; NaN ; NaN ];
omc_error_33 = [ 6.536854e-02 ; 6.142477e-02 ; 1.254662e-01 ];
Tc_error_33  = [ 1.034190e+03 ; 1.635550e+03 ; 1.727585e+03 ];

%-- Image #34:
omc_34 = [ NaN ; NaN ; NaN ];
Tc_34  = [ NaN ; NaN ; NaN ];
omc_error_34 = [ 6.483283e-02 ; 5.745770e-02 ; 1.770124e-01 ];
Tc_error_34  = [ 1.109176e+03 ; 1.749496e+03 ; 1.808938e+03 ];

%-- Image #35:
omc_35 = [ NaN ; NaN ; NaN ];
Tc_35  = [ NaN ; NaN ; NaN ];
omc_error_35 = [ 8.755366e-02 ; 3.292425e-02 ; 1.722640e-01 ];
Tc_error_35  = [ 1.098737e+03 ; 1.737897e+03 ; 1.776814e+03 ];

%-- Image #36:
omc_36 = [ 1.334354e+00 ; -1.284072e+00 ; 1.480339e+00 ];
Tc_36  = [ 2.035697e+03 ; 8.610535e+02 ; -5.685631e+03 ];
omc_error_36 = [ 6.866819e-02 ; 9.861136e-02 ; 1.822259e-01 ];
Tc_error_36  = [ 1.064894e+03 ; 1.684622e+03 ; 1.734896e+03 ];

%-- Image #37:
omc_37 = [ NaN ; NaN ; NaN ];
Tc_37  = [ NaN ; NaN ; NaN ];
omc_error_37 = [ 6.702845e-02 ; 9.839188e-02 ; 1.833992e-01 ];
Tc_error_37  = [ 1.064687e+03 ; 1.684102e+03 ; 1.732958e+03 ];

%-- Image #38:
omc_38 = [ NaN ; NaN ; NaN ];
Tc_38  = [ NaN ; NaN ; NaN ];
omc_error_38 = [ 1.159454e-01 ; 8.196901e-02 ; 1.285394e-01 ];
Tc_error_38  = [ 1.078032e+03 ; 1.729735e+03 ; 1.789321e+03 ];

%-- Image #39:
omc_39 = [ NaN ; NaN ; NaN ];
Tc_39  = [ NaN ; NaN ; NaN ];
omc_error_39 = [ 5.246180e-02 ; 6.595210e-02 ; 1.342782e-01 ];
Tc_error_39  = [ 1.006514e+03 ; 1.594007e+03 ; 1.684636e+03 ];

%-- Image #40:
omc_40 = [ NaN ; NaN ; NaN ];
Tc_40  = [ NaN ; NaN ; NaN ];
omc_error_40 = [ 5.760968e-02 ; 5.893926e-02 ; 1.305929e-01 ];
Tc_error_40  = [ 1.000903e+03 ; 1.584075e+03 ; 1.677753e+03 ];

%-- Image #41:
omc_41 = [ NaN ; NaN ; NaN ];
Tc_41  = [ NaN ; NaN ; NaN ];
omc_error_41 = [ 6.632688e-02 ; 1.403090e-01 ; 2.578870e-01 ];
Tc_error_41  = [ 9.886228e+02 ; 1.527070e+03 ; 1.847193e+03 ];

