CC = g++
CPPFLAG =  `pkg-config --cflags --libs opencv`
BASE_DIR=.
SOURCE=$(BASE_DIR)
INCLUDE += -I$(BASE_DIR) -I/Users/Lucky/Downloads/vlc/include/

SRC= $(SOURCE)/main.o 
OBJECTS = $(patsubst %.cpp,%.o,$(SRC))
TARGET=camtrack
all: $(TARGET) 
$(TARGET):$(OBJECTS) 
	$(CC)  $(OBJECTS)  $(CPPFLAG) -o $(TARGET)
$(OBJECTS):%.o : %.cpp
	$(CC) -c $(CPPFLAG) $(INCLUDE) $< -o $@
clean:
	rm -rf  $(OBJECTS) 
