
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cstdlib>
#include <cmath>

// OPENCV IMPORTS
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>


using namespace cv;
using namespace std;

Ptr<BackgroundSubtractor> bg =  new BackgroundSubtractorMOG2();
Mat frame;
Mat fgMask;
int morph_size = 20;
int morph_operator = 2;
int max_operator = 4;
int max_elem = 2;
int max_kernel_size = 21;
Mat element = getStructuringElement(MORPH_RECT, Size(2 * morph_size + 1, 2 * morph_size + 1), Point(morph_size, morph_size));

	
int carSize_lower = 1000;			// LOWER THRESHOLD FOR CAR SIZE
int carSize_upper = 2000;			// UPPER THRESHOLD FOR CAR SIZE

int thresh_red = 50;				// THRESHOLD FOR RED COLOR
int frameNum = 0;
vector<float> blobArea;
double seconds;	
time_t startTime;
time_t startTimeMove;
int frameCount = 1;
int keyboard = 0;
bool captured = false;



int captureImages(Mat frame){
	time_t currentTime = time(NULL);
	seconds = difftime(currentTime, startTime);
	if (seconds > 2){
		std::ostringstream filename;
		filename << "calib_images_2/image" << frameCount << ".jpg";
		imwrite(filename.str().c_str(), frame);
		frameCount++;
		startTime = time(NULL);
	}
	return 0;
}

int captureFrame(Mat frame){
	
	std::ostringstream filename;
	filename << "mechError/image" << frameCount << ".jpg";
	imwrite(filename.str().c_str(), frame);	
	cout << "Saved frame " << filename.str().c_str() << "\n";
	frameCount++;
	return 0;
}




int main(int argc, char* argv[]) 
{
	//  string videoFileName = "rtsp://192.168.0.20:554/stream1";
	string videoFileName = "rtsp://bigbrother.winlab.rutgers.edu:554/stream1";
	// string videoFileName = "rtsp://root:root123@173.72.36.221:443/videoMain";
	// string videoFileName = "http://173.72.36.221:443/cgi-bin/CGIProxy.fcgi?cmd=setSubStreamFormat&format=1&usr=root&pwd=root123 ";


	// viewFrames(videoFileName);
	
	VideoCapture capture(videoFileName);

	if (!capture.isOpened()){
		std::cout << "Unable to open video file: " << videoFileName << endl;
		exit(EXIT_FAILURE);
	}
	
	if (!capture.read(frame)) {
		cout << "Unable to read next frame." << endl;
		cout << "Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	double fps = capture.get(CV_CAP_PROP_FPS);


	//VideoWriter video("./zoomVideo.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps, Size(round(frame.cols/2), round(frame.rows/2)), true);


	startTime = time(NULL);

	
  while(1){

	  if (!capture.read(frame)) {
		cout << "Unable to read next frame." << endl;
		cout << "Exiting..." << endl;
		exit(EXIT_FAILURE);
	}


	time_t currentTime = time(NULL);
	seconds = difftime(currentTime, startTime);	
	// cout << "Seconds since start - " << seconds << "\n";
	
	frameNum++;
    if(frame.empty())
    {
        printf("Cannot read image file: \n");
        return -1;
    }
	
	//Mat opFrame;
	//resize(frame, opFrame, Size(round(frame.cols/2), round(frame.rows/2))); 
	
	

	captureImages(frame);

	


	waitKey(10);

 }

	return 0;
}