%% Camera coordinate system transformation

close all; clear all

aovH = 55.4;

% imref = imread('imPan0.jpg');
% [rows columns depth]=size(imref);
% interPixelAngleH = aovH/columns;
% interPixelAngleV = interPixelAngleH;
% 
% figure; im_new = image(imref, 'XData', [-columns/2 columns/2], 'YData', [-rows/2 rows/2]);axis xy
% hold on; grid on;
% 
% sphcoordRef = zeros(rows, columns, depth);
% 
% for k = 1:1
%     [i j] = ginput(1);
%     rho = 1;
%     theta = (ceil(j)*interPixelAngleV);               % elevation
%     phi = (ceil(i)*interPixelAngleH)                 % azimuth
% %     [x,y,z] = sph2cart(phi,theta,rho)
% end
% 
% %% Camera Motion
% 
% im1 = imread('imPan0-1.jpg');
% 
% camMotion = -0.1;
% numSteps = camMotion/0.1;
% perStepPan = (18);
% perStepTilt = 0;
% 
% panShift = numSteps*perStepPan;
% 
% figure; im_new1 = image(im1, 'XData', [-columns/2 columns/2], 'YData', [-rows/2 rows/2]);axis xy
% hold on; grid on;
% 
% for k1 = 1:1
%     [i1 j1] = ginput(1);
%     rho = 1;
%     theta = (ceil(j1)*interPixelAngleV);      % elevation
%     phi = (ceil(i1)*interPixelAngleH)+panShift                 % azimuth
% %     [x,y,z] = sph2cart(phi,theta,rho)    
% end


%% ZOOM

aovH = 55.4;

imref = imread('im010.jpg');
[rows columns depth]=size(imref);
interPixelAngleH = aovH/columns;
interPixelAngleV = interPixelAngleH;

figure; im_new = image(imref, 'XData', [-columns/2 columns/2], 'YData', [-rows/2 rows/2]);axis xy
hold on; grid on;

leastZoomUnit = 1/20;
camZoom = 0;
zoomFactor = (camZoom/leastZoomUnit)+1;

for k = 1:1
    [i j] = ginput(1);
    rho = zoomFactor
    theta = (ceil(j)*interPixelAngleV)               % elevation
    phi = (ceil(i)*interPixelAngleH)                 % azimuth
end


im1 = imread('im010-05.jpg');

camZoom = 0.05;
zoomFactor = (camZoom/leastZoomUnit)+1;

figure; im_new = image(im1, 'XData', [-columns/2 columns/2], 'YData', [-rows/2 rows/2]);axis xy
hold on; grid on;

for k = 1:1
    [i j] = ginput(1);
    rho = zoomFactor
    theta = (ceil(j)*interPixelAngleV)/zoomFactor               % elevation
    phi = (ceil(i)*interPixelAngleH)/zoomFactor               % azimuth
end

im2 = imread('im010-1.jpg');

camZoom = 0.1;
zoomFactor = (camZoom/leastZoomUnit)+1;

figure; im_new = image(im2, 'XData', [-columns/2 columns/2], 'YData', [-rows/2 rows/2]);axis xy
hold on; grid on;

for k = 1:1
    [i j] = ginput(1);
    rho = zoomFactor
    theta = ceil((j)*interPixelAngleV)/zoomFactor               % elevation
    phi = ceil((i)*interPixelAngleH)/zoomFactor               % azimuth
end












