
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cstdlib>
#include <cmath>

// OPENCV IMPORTS
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/background_segm.hpp>


using namespace cv;
using namespace std;

Ptr<BackgroundSubtractor> bg =  new BackgroundSubtractorMOG2();
Mat frame;
Mat fgMask;
int morph_size = 20;
int morph_operator = 2;
int max_operator = 4;
int max_elem = 2;
int max_kernel_size = 21;
Mat element = getStructuringElement(MORPH_RECT, Size(2 * morph_size + 1, 2 * morph_size + 1), Point(morph_size, morph_size));

	
int carSize_lower = 1000;			// LOWER THRESHOLD FOR CAR SIZE
int carSize_upper = 2000;			// UPPER THRESHOLD FOR CAR SIZE

int thresh_red = 50;				// THRESHOLD FOR RED COLOR
int frameNum = 0;
vector<float> blobArea;
double seconds;	
time_t startTime;
time_t startTimeMove;
int frameCount = 1;
int keyboard = 0;
bool captured = false;

int getContours(Mat thisFrame, Mat mask){

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	vector<Point2f> pts;

	vector<Point> contours_poly;
	Rect boundRect;
	RotatedRect fittingRect;
	Point2f center;
	float radius;
	Mat thisFrameGray;

	cvtColor(thisFrame, thisFrameGray, CV_BGR2GRAY);

	findContours(mask, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	vector<Moments> mu(contours.size());
	vector<Point2f> mc(contours.size());

	for (int i = 0; i < contours.size(); i++){
		Mat pntsInContour;
		mu[i] = moments(contours[i], false);
		mc[i] = Point2f(mu[i].m10 / mu[i].m00, mu[i].m01 / mu[i].m00);
		double area = contourArea(contours.at(i));
		approxPolyDP(Mat(contours[i]), contours_poly, 3, true);
		boundRect = boundingRect(Mat(contours_poly));
		fittingRect = minAreaRect(Mat(contours_poly));
		minEnclosingCircle((Mat)contours_poly, center, radius);

		if (area > carSize_lower && area< carSize_upper){
			blobArea.push_back(area);
			fillConvexPoly(mask, &contours_poly[0], (int)contours_poly.size(), Scalar(255, 0, 0), 8, 0);
			rectangle(thisFrame, boundRect.tl(), boundRect.br(), Scalar(0, 255, 0), 5, 8, 0);

		}
		imshow("FRAME", thisFrame);
	}

	return 0;
}


bool isCarRed(){
	bool redCar = false;
	Mat planes[3], red, gray, diffFrame, finalRed, morphed;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	vector<Point> contours_poly;
	Rect boundRect;

	split(frame, planes);
	cvtColor(frame, gray, CV_BGR2GRAY);
	subtract(planes[2], gray, diffFrame);
	threshold(diffFrame, red, thresh_red, 255, THRESH_BINARY);


	findContours(red, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	

	for (int i = 0; i < contours.size(); i++){
		double area = contourArea(contours.at(i));
		if (area > carSize_lower){
			approxPolyDP(Mat(contours[i]), contours_poly, 3, true);
			boundRect = boundingRect(Mat(contours_poly));
			rectangle(frame, boundRect.tl(), boundRect.br(), Scalar(0, 0, 255), 5, 8, 0);
			imshow("FRAME", frame);
			redCar = true;
		}
		else
			redCar = false;
	}

	return redCar;
}

int viewFrames(string videoFileName){
	VideoCapture capture(videoFileName);
	 while(1){
	capture.read(frame);
	imshow("FRAME", frame);
	bg->operator()(frame, fgMask, .1);
	erode(fgMask, fgMask, Mat());
	dilate(fgMask, fgMask, Mat());
	morphologyEx(fgMask, fgMask, 3, element);
		
	//imshow("MASK",fgMask);
	getContours(frame, fgMask);
	if (blobArea.size() > 0){
		float currentArea = blobArea.at(blobArea.size()-1);
		blobArea.clear();
		if (currentArea > carSize_lower){
		std::ostringstream filename;
		filename << "motionCaptures/deer-" << frameNum << ".jpg";
		imwrite(filename.str().c_str(), frame);
	 }
	}	
	 
	waitKey(30);
	 }
	return 0;
}

int captureImages(Mat frame){
	time_t currentTime = time(NULL);
	seconds = difftime(currentTime, startTime);
	if (seconds > 2){
		std::ostringstream filename;
		filename << "mechError/image" << frameCount << ".jpg";
		imwrite(filename.str().c_str(), frame);
		frameCount++;
		startTime = time(NULL);
	}
	return 0;
}

int captureFrame(Mat frame){
	
	std::ostringstream filename;
	filename << "mechError/image" << frameCount << ".jpg";
	imwrite(filename.str().c_str(), frame);	
	cout << "Saved frame " << filename.str().c_str() << "\n";
	frameCount++;
	return 0;
}


int moveCamera(float pan, float tilt, float zoom){
	std::ostringstream moveCommand;
	moveCommand << "../camControl/ipconvif bigbrother.winlab.rutgers.edu " << pan << " " << tilt << " " << zoom;
	cout << moveCommand.str().c_str() << "\n";
	system(moveCommand.str().c_str());
	return 0;
}

bool AlmostEqualRelative(float A, float B, float maxRelDiff = FLT_EPSILON)
{
    // Calculate the difference.
    float diff = fabs(A - B);
    A = fabs(A);
    B = fabs(B);
    // Find the largest
    float largest = (B > A) ? B : A;
 
    if (diff <= largest * maxRelDiff)
        return true;
    return false;
}


int main(int argc, char* argv[]) 
{
	//  string videoFileName = "rtsp://192.168.0.20:554/stream1";
	string videoFileName = "rtsp://bigbrother.winlab.rutgers.edu:554/stream1";
	// string videoFileName = "rtsp://root:root123@173.72.36.221:443/videoMain";
	// string videoFileName = "http://173.72.36.221:443/cgi-bin/CGIProxy.fcgi?cmd=setSubStreamFormat&format=1&usr=root&pwd=root123 ";


	// viewFrames(videoFileName);
	
	VideoCapture capture(videoFileName);

	if (!capture.isOpened()){
		std::cout << "Unable to open video file: " << videoFileName << endl;
		exit(EXIT_FAILURE);
	}
	
	if (!capture.read(frame)) {
		cout << "Unable to read next frame." << endl;
		cout << "Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	double fps = capture.get(CV_CAP_PROP_FPS);


	//VideoWriter video("./zoomVideo.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps, Size(round(frame.cols/2), round(frame.rows/2)), true);


	startTime = time(NULL);
	float defaultPan = -0.7;
	float defaultTilt = 1;
	float defaultZoom = 0;
	
	float pan = -1;
	moveCamera(pan, defaultTilt, defaultZoom);
	
  while(1){

	  if (!capture.read(frame)) {
		cout << "Unable to read next frame." << endl;
		cout << "Exiting..." << endl;
		exit(EXIT_FAILURE);
	}


	time_t currentTime = time(NULL);
	seconds = difftime(currentTime, startTime);	
	// cout << "Seconds since start - " << seconds << "\n";
	
	frameNum++;
    if(frame.empty())
    {
        printf("Cannot read image file: \n");
        return -1;
    }
	
	Mat opFrame;
	resize(frame, opFrame, Size(round(frame.cols/2), round(frame.rows/2))); 
	
	if (seconds > 6 && pan <= 1) {
		pan = pan+0.1;
		cout<<"pan = " << pan << "\n";
		moveCamera(pan, defaultTilt, defaultZoom);
		startTime = time(NULL);
		startTimeMove = time(NULL);
		captured = false;
	}
		
	double secondsSinceMove = difftime(time(NULL), startTimeMove);
	// cout << "Seconds since move:  " << secondsSinceMove << "\n";
	
	if (secondsSinceMove > 3 && pan <= 1 && captured == false){
		if (AlmostEqualRelative(pan, defaultPan, FLT_EPSILON)){
			cout << "Found start position\n";
			captureFrame(opFrame);
			captured = true;
		}

	}
	
	 if (pan >= 1)
	 	pan = -1.1;


	//captureImages(frame);

	

	// video.write(opFrame);
	
	// // MOSAIC CAPTURE
	// if (seconds > 30 && zoom <= 1) {
	// 	zoom = zoom+0.1;
	// 	cout<<"zoom = " << zoom << "\n";
	// 	std::ostringstream moveCommand;
	// 	moveCommand << "../camControl/ipconvif bigbrother.winlab.rutgers.edu " << pan << " " << tilt << " " << zoom;
	// 	cout << moveCommand.str().c_str() << "\n";
	// 	system(moveCommand.str().c_str());
	// 	startTime = time(NULL);
	// }
	
	// if (zoom >= 1)
	// 	break;
	
	
	
	
	
	// CAR DETECTION
// 	bg->operator()(opFrame, fgMask, .1);
// 	erode(fgMask, fgMask, Mat());
// 	dilate(fgMask, fgMask, Mat());
// 	morphologyEx(fgMask, fgMask, 3, element);
	
	
// 	imshow("MASK",fgMask);
//     getContours(opFrame, fgMask);
// 	frame = opFrame;
// 	if (blobArea.size() > 0){
// 		float currentArea = blobArea.at(blobArea.size()-1);
// 		if (currentArea > carSize_lower){
// 			std::ostringstream filename;
// 			filename << "motionCaptures/motion-" << frameNum << ".jpg";
// 			imwrite(filename.str().c_str(), frame);
// 		}
		
// 	}
// 	// bool redCar = isCarRed();
// 	// if (redCar)
// 		// cout << "RED CAR" << endl;

// //get the frame number and write it on the current frame
// stringstream ss;
// rectangle(frame, cv::Point(10, 2), cv::Point(100,20), cv::Scalar(255,255,255), -1);
// ss << capture.get(CV_CAP_PROP_POS_FRAMES);
// string frameNumberString = ss.str();
// putText(opFrame, frameNumberString.c_str(), cv::Point(15, 15),
//         FONT_HERSHEY_SIMPLEX, 0.5 , cv::Scalar(0,0,0));

// 	imshow("FRAME",opFrame);
	

	waitKey(10);

 }
 	// video.release();
	return 0;
}